var chartObj = [];
var refTreeNames = [];
var refTree;
var selectedNode = null;
outer_update = null;

//Using queue for asynchronous file loading
//http://bl.ocks.org/mapsam/6090056
$(document).foundation();
$(document).on('opened', '[data-reveal]', function() {
  var element = $(".inputName:visible").first();
  element.focus(function() {
    this.selectionStart = this.selectionEnd = this.value.length;
  });
  element.focus();
});
$('#RenameNodeForm').submit(function(e) {
  rename_node();
  return false;
});
$('#CreateNodeForm').submit(function(e) {
  create_node();
  return false;
});

function close_modal() {
  $(document).foundation('reveal', 'close');
}

function generateUUID() {
  var d = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
};

function create_node() {
  if (create_node_parent && create_node_modal_active) {
    if (create_node_parent._children != null) {
      create_node_parent.children = create_node_parent._children;
      create_node_parent._children = null;
    }
    if (create_node_parent.children == null) {
      create_node_parent.children = [];
    }
    id = generateUUID();
    name = $('#CreateNodeName').val();
    new_node = {
      'name': name,
      'id': id,
      'depth': create_node_parent.depth + 1,
      'children': [],
      '_children': null
    };
    console.log('Create Node name: ' + name);
    create_node_parent.children.push(new_node);
    create_node_modal_active = false;
    $('#CreateNodeName').val('');
    console.log(create_node_parent);
  }
  close_modal();
  outer_update(create_node_parent);
}



function rename_node() {
  if (node_to_rename && rename_node_modal_active) {
    name = $('#RenameNodeName').val();
    console.log('New Node name: ' + name);
    node_to_rename.name = name;
    rename_node_modal_active = false;

  }
  close_modal();
  outer_update(node_to_rename);
}

d3.queue()
  .defer(d3.json, "data/taxonomy_tree.json")
  .defer(d3.json, "data/base_tree.json")
  .await(createChart);

function createChart(error, refData, baseData) {


  var refView = taxonomyViewer()
    .enableDrag(false);

  var baseView = taxonomyViewer()
  //.height(1080);

  var container = d3.select("#reftree")
    .datum(baseData)
    .call(refView);

  var container = d3.select("#basetree")
    .datum(refData)
    .call(baseView);
}

//simultaneous scroll the ref and base divs
//use sync variable for smooth scroll on both divs
var isSyncingLeftScroll = false;
var isSyncingRightScroll = false;

var refDiv = document.getElementById("reftree")
var baseDiv = document.getElementById("basetree")

//initialize the Synchronous scroll
syncScroll(true, false);

d3.select("#syncscroll").on('change', changeSync);
d3.select("#noscroll").on('change',  changeSync);

function changeSync(){
  if(d3.select("#syncscroll").property("checked")){
    syncScroll(true, false);
  } else if( d3.select("#noscroll").property("checked") ){
    syncScroll(false, true);
  }
}//end of changesync()

function syncScroll(firstSync,  secondSync) {

  refDiv.onscroll = function() {
    if (!isSyncingLeftScroll) {
      isSyncingRightScroll = firstSync;
      baseDiv.scrollTop = this.scrollTop;
    }
    isSyncingLeftScroll = secondSync;
  }

  baseDiv.onscroll = function() {
    if (!isSyncingRightScroll) {
      isSyncingLeftScroll = firstSync;
      refDiv.scrollTop = this.scrollTop;
    }
    isSyncingRightScroll = secondSync;
  }

}//end of syncScroll

d3.select("#fullscreen").on("click", function(){
  var elem = document.getElementById("fullscreen");
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  }
 })
