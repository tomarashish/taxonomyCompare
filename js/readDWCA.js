var chartObj = [];
var refTreeNames = [];
var baseTreeNames = [] ;
var refTree, refTreeLevel = 2, baseTreeLevel = 2;
var selectedNode = null;
var outer_update = null;

$(document).foundation();
$(document).on('opened', '[data-reveal]', function() {
  var element = $(".inputName:visible").first();
  element.focus(function() {
    this.selectionStart = this.selectionEnd = this.value.length;
  });
  element.focus();
});
$('#RenameNodeForm').submit(function(e) {
  rename_node();
  return false;
});
$('#CreateNodeForm').submit(function(e) {
  create_node();
  return false;
});

function close_modal() {
  $(document).foundation('reveal', 'close');
}

function generateUUID() {
  var d = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
};

function close_modal() {
        $(document).foundation('reveal', 'close');
}

function create_node() {
  if (create_node_parent && create_node_modal_active) {
    if (create_node_parent._children != null) {
      create_node_parent.children = create_node_parent._children;
      create_node_parent._children = null;
    }
    if (create_node_parent.children == null) {
      create_node_parent.children = [];
    }
    id = generateUUID();
    name = $('#CreateNodeName').val();
    new_node = {
      'data': {'key': name },
      'id': id,
      'depth': create_node_parent.depth + 1,
      'height': create_node_parent.height - 1,
      'children': null,
      '_children': null,
      'parent': create_node_parent,
    };

    create_node_parent.children.push(new_node);
    create_node_modal_active = false;
    $('#CreateNodeName').val('');

  }
  close_modal();
  outer_update(create_node_parent);
}



function rename_node() {
  if (node_to_rename && rename_node_modal_active) {
    name = $('#RenameNodeName').val();
    node_to_rename.data.key = name;
    rename_node_modal_active = false;

  }
  close_modal();
  outer_update(node_to_rename);
}

/* Level select Drop down */
$('#baseLevel li').click(function(){
  $('#baseLevel li').removeClass("selected");
    $(this).addClass("selected");

     baseTreeLevel = $('#baseLevel li.selected').text();
});

$('#refLevel li').click(function(){
  $('#refLevel li').removeClass("selected");
    $(this).addClass("selected");

     refTreeLevel = $('#refLevel li.selected').text();
});

function getTaxonLevels(csv_data) {

  var keys = [];
  //if (csv_data[0]["Family"])

  if ("kingdom" in csv_data[0])
    keys.push("kingdom")

  if ("phylum" in csv_data[0])
    keys.push("phylum")

  if ("class" in csv_data[0])
    keys.push("class")

  if ("order" in csv_data[0])
    keys.push("order")

  if ("family" in csv_data[0])
    keys.push("family")

  if ("genus" in csv_data[0])
    keys.push("genus")

  if ("species" in csv_data[0])
      keys.push("species")
  else {
      keys.push("acceptedNameUsage")
    }

  return keys;
}


function groupAsTree(data, taxonLevels) {

  var nest = d3.nest();
  var treeData;

  taxonLevels.forEach(function (k) {
    treeData = nest.key(function (d) {
      //console.log(d[k]);
      return d[k];
    })
    .rollup(function(v){
      return v.length;
    })
  });


  var root = {
    "key": "Root",
    "values": treeData.entries(data) //compute the nest
  }
  //console.log(root);
  return root;
}


d3.tsv("./data/dyntaxa_dwca.tsv", function (error, taxoData) {

  //console.log(JSON.stringify(groupAsTree(taxoData)));
  //console.log(groupAsTree(taxoData));

  var taxonKeys = getTaxonLevels(taxoData);
  //console.log(groupAsTree(taxoData, taxonKeys))
  var baseView = taxonomyViewer()
    .enableDrag(true);

  var container = d3.select("#basetree")
    .datum(groupAsTree(taxoData, taxonKeys))
    .call(baseView);

});

//file upload
var raw_data,
  try_example = false,
  upload_file = false,
  upload_url = false;

var reader = new FileReader();

d3.select("#submit_file").on("click", function () {
  //console.log(d3.select("#upload_file")[0][0].files[0])
  console.log(d3.select("#upload_file"));
  reader.readAsText(d3.select("#upload_file")._groups[0][0].files[0]);

  reader.onload = function (event) {
    var raw_data = event.target.result;
    upload_file = true;
    console.log(raw_data);
    var taxonKeys = getTaxonLevels(raw_data);
    //console.log(groupAsTree(taxoData, taxonKeys))
    var refView = taxonomyViewer()
      .enableDrag(false);

      //remove previous svg
      //d3.select("#reftree").remove();

    var container = d3.select("#reftree")
        .datum(groupAsTree(taxoData, taxonKeys))
        .call(refView);

    };
});

loadExampleTree();

d3.select("#loadExample").on("click", loadExampleTree)

function loadExampleTree(){
  d3.tsv("data/occurrence.tsv", function (error, taxoData) {

    var taxonKeys = getTaxonLevels(taxoData);
    //console.log(groupAsTree(taxoData, taxonKeys))
    var refView = taxonomyViewer()
      .enableDrag(false);

    var container = d3.select("#reftree")
        .datum(groupAsTree(taxoData, taxonKeys))
        .call(refView);

    });
}//end of load example

//simultaneous scroll the ref and base divs
//use sync variable for smooth scroll on both divs
var isSyncingLeftScroll = false;
var isSyncingRightScroll = false;

var refDiv = document.getElementById("reftree")
var baseDiv = document.getElementById("basetree")

//initialize the Synchronous scroll
syncScroll(true, false);

d3.select("#syncscroll").on('change', changeSync);
d3.select("#noscroll").on('change',  changeSync);

function changeSync(){
  if(d3.select("#syncscroll").property("checked")){
    syncScroll(true, false);
  } else if( d3.select("#noscroll").property("checked") ){
    syncScroll(false, true);
  }
}//end of changesync()

function syncScroll(firstSync,  secondSync) {

  refDiv.onscroll = function() {
    if (!isSyncingLeftScroll) {
      isSyncingRightScroll = firstSync;
      if(isSyncingRightScroll)
        baseDiv.scrollTop = this.scrollTop;
    }
    isSyncingLeftScroll = secondSync;
  }

  baseDiv.onscroll = function() {
    if (!isSyncingRightScroll) {
      isSyncingLeftScroll = firstSync;
        if(isSyncingLeftScroll)
          refDiv.scrollTop = this.scrollTop;
    }
    isSyncingRightScroll = secondSync;
  }

}//end of

function flatObjectToString(obj) {
    var s = "";
    Object.keys(obj).map(key => {
      if (obj[key] === null) {
        s += key + ":";
      } else if (obj[key].toLocaleDateString) {
        s += key + ": " + obj[key].toLocaleDateString() + "\n";
      } else if (obj[key] instanceof Array) {
        s += key + ":\n" + listToFlatString(obj[key]);
      } else if (typeof obj[key] == "object") {
        s += key + ":\n" + flatObjectToString(obj[key]);
      } else {
        s += key + ":" + obj[key];
      }
      s += "\n";
    });
    return s;
  }

  function listToFlatString(list) {
    var s = "";
    list.map(item => {
      Object.keys(item).map(key => {
        s += "";
        if (item[key] instanceof Array) {
          s += key + "\n" + listToFlatString(item[key]);
        } else if (typeof item[key] == "object" && item[key] !== null) {
          s += key + ": " + flatObjectToString(item[key]);
        } else {
          s += key + ": " + (item[key] === null ? "" : item[key].toLocaleDateString ? item[key].toLocaleDateString : item[key].toString());
        }
        s += "\n";
      });
    });
    return s;
  }

  function flatten(object, addToList, prefix) {
    Object.keys(object).map(key => {
      if (object[key] === null) {
        addToList[prefix + key] = "";
      } else
      if (object[key] instanceof Array) {
        addToList[prefix + key] = listToFlatString(object[key]);
      } else if (typeof object[key] == 'object' && !object[key].toLocaleDateString) {
        flatten(object[key], addToList, prefix + key + '.');
      } else {
        addToList[prefix + key] = object[key];
      }
    });
    return addToList;
  }
