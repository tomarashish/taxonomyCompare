/**
 * inspiration from
 * http://bl.ocks.org/mbostock/4339083
 */

taxonomyViewer = function module(){

var tree = d3.tree()
    .size([0, 250]);
/* d3.layout.tree()
    .nodeSize([0, 50])
    .value(function (d) {
      return 1;
    })
    .children(function (d, i) {
      if(d.key)
        return d.values;
      //return (!d.values || d.values.length === 0) ? null : d.values;
    });
    //.sort(function(a, b) { return a.name.toLowerCase().localeCompare(b.name.toLowerCase());})
*/
   var margin = {top: 50, right: 20, bottom: 30, left: 70},
    width = 600 - margin.left - margin.right,
       height = 30000 - margin.top - margin.bottom,
       barHeight = 30,
    barWidth = 400 * .8;

   var i = 0,
    duration = 100,
    root, svg, svgGroup, height, sortTree = true;

   // Calculate total nodes, max label length
    var totalNodes = 0;
    var enableDrag = true;
    var nodeEnter;
    var maxLabelLength = 0;
    var new_root;
    // variables for drag/le

    var draggingNode = null;
    //var dragNodedata = null
    // panning variables
    var panSpeed = 200;
    var panBoundary = 20; // Within 20px from edges will pan when dragging.
    // Misc. variables
    var i = 0;
// var color = d3.scaleOrdinal().range(d3.quantize(d3.interpolateRainbow, data.children.length + 1))color = d3.scaleOrdinal().range(d3.quantize(d3.interpolateRainbow, data.children.length + 1))

   var overCircle = function(d) {
       //console.log(d)
        selectedNode = d;
        updateTempConnector();
    };
    var outCircle = function(d) {
        selectedNode = null;
        updateTempConnector();
    };


   var menu = [
            {
                    title: 'Rename node',
                    action: function(elm, d, i) {
                            console.log('Rename node');
                            $("#RenameNodeName").val(d.name);
                            rename_node_modal_active = true;
                            node_to_rename = d
                            $("#RenameNodeName").focus();
                            $('#RenameNodeModal').foundation('reveal', 'open');
                    }
            },
            {
                    title: 'Delete Node',
                    action: function(elm, d, i) {
                            console.log('Delete node');
                            delete_node(d);
                    }
            },
            /*{
                    title: 'Select Sub Tree',
                    action: function(elm, d, i) {
                            console.log('Highlight Node');
                            //highlightNode(d);
                    }
            },*/
            {
                    title: 'Create child node',
                    action: function(elm, d, i) {
                            console.log('Create child node');
                            create_node_parent = d;
                            create_node_modal_active = true;
                            $('#CreateNodeModal').foundation('reveal', 'open');
                            $('#CreateNodeName').focus();

                    }
            }
    ];


  // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
  //var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

  function exports(_selection){
    _selection.each(function(_data){

    //  tree_data =  _data;
      //remove previously drawn svg element
      d3.select("#" +this.id + "Svg").remove();

      //create a new svg elemnt
      svg = d3.select('body').select("#"+this.id)
        .append("svg")
        .attr("id", this.id + "Svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Append a group which holds all nodes and which the zoom Listener can act upon.
      svgGroup = svg.append("g") .attr("id", this.id+"SvgGroup");

    //create heirarchy object from the csv nested data data
    root = d3.hierarchy(_data, function (d) {
        return d.values;
      })
      .sum(function (d) {
        return 1;
      });

      root.x0 = 0;
      root.y0 = 0;

      select2_data = extract_select2_data(_data,[],0)[1];//I know, not the prettiest...
      //childrens = getAllchildrenNames(select2_data)
      //

      collapseAll(root)
      expand(root)
      update(root);

      new_root = $.extend(true,{},root);

      chartObj.push(root)
      if(!enableDrag){
      //getBaseName(root)
      }

      if(enableDrag == false){

        $('#resetBase').on('click', resetTreeView);

        baseTreeNames = getAllchildrenNames(getNodeNames(_data,[],0)[1]);
        //  d3.select("#sortBaseTree").on("change",update);
        d3.select('#expandBase').on('click',  function(){
            clearClass(chartObj[1])
            expand(root, baseTreeLevel)
            update(root);
          });

        d3.select('#collapseBase').on('click', function(){
          clearClass(root)
          collapseAll(root)
          expand(root)
          update(root);
        });

        $('#searchBase .typeahead').typeahead('destroy');

        var childrens = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.whitespace,
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          // `getAllchildrenNames` returns an array of names from select2_data
          local: getAllchildrenNames(select2_data)
          });

          $('#searchBase .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
          },
          {
            name: 'childrens',
            limit: 50,
            source: childrens
          });
            //.on("typeahead:selected", function (obj, name) {
            //  $SelectedCompany.html("Selected Company: " + JSON.stringify(company)).show();
          //});


          d3.select("#searchBaseTree").on("click", function(){

            clearClass(root);
            collapseAll(root);
            update(root);

            var typeahead = document.querySelector('#baseInput').value;

              var paths = searchTree(root,typeahead,[]);
		          if(typeof(paths) !== "undefined"){
			             openPaths(paths);
		          }
		          else{
			             alert(e.object.text+" not found in working taxonomy!");
		              }
          });
      }//end of condition

      if(enableDrag == true){


          $('#resetRef').on('click', resetTreeView);

          refTreeNames = getAllchildrenNames(getNodeNames(_data,[],0)[1]);
        /*d3.select("#sortRefTree").on("change", function(){
          console.log("working");
          if( d3.select("#sortRefTree").property("checked")){
            chartObj[1].__data__.children.sort(function(a, b) { return a.name.toLowerCase().localeCompare(b.name.toLowerCase());});
            update(chartObj[0])
          }
      });*/

      $('#searchBase .typeahead').typeahead('destroy');

      var childrens = new Bloodhound({

        // `getAllchildrenNames` returns an array of names from select2_data
        local: getAllchildrenNames(select2_data),
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace

        });

        $('#searchRef .typeahead').typeahead({
          hint: true,
          highlight: true,
          minLength: 1
        },
        {
          name: 'childrens',
          limit: 50,
          source: childrens
        });

        d3.select("#searchRefTree").on("click", function(){

          clearClass(root);
          collapseAll(root);
          update(root);

          var typeahead = document.querySelector('#refInput').value;

            var paths = searchTree(root,typeahead,[]);
            if(typeof(paths) !== "undefined"){
                 openPaths(paths);
            }
            else{
                 alert( typeahead +" not found in Reference Taxonomy!");
                }
        });

        d3.select('#expandRef').on('click',  function(){
          clearClass(root);
          expand(root, refTreeLevel)
          update(root);
        });

        d3.select('#collapseRef').on('click', function(){

          clearClass(root)
          collapseAll(root)
          expand(root)
          update(root);
        });
      }

      //var flatJSON = flatObjectToString(root.children)
      //console.log(flatJSON);
        //var csv = Papa.unparse(flatJSON);
        //console.log(csv);
    })//end of selections
  }//end of exports

  //Setting reset and image save button as d3 elemen

    function resetTreeView(){

      //create heirarchy object from the csv nested data data
      root = d3.hierarchy(new_root.data, function (d) {
          return d.values;
        })
        .sum(function (d) {
          return 1;
        });

        root.x0 = 0;
        root.y0 = 0;

        collapseAll(root)
        expand(root, 1)
        update(root);
      }

      //function
  function clearClass(d) {
    d.class = "";
    if (d.children)
      d.children.forEach(clearClass);
    else if (d._children)
      d._children.forEach(clearClass);
  }

function expandAll(root_data, maxLevel ){
  expand(root_data,3)
  update(root_data);
}

function getAllchildrenNames(arrayNamesList){
  var namesArray = []

   arrayNamesList.forEach(function(d){
     namesArray.push(d.text)
   });

  return namesArray;
}

function update(source) {

  //height = Math.max(500, nodes.length * barHeight + margin.top + margin.bottom );
  //root.children.sort(function(a, b) { return d3.ascending(a.name, b.name); });

    nodes = tree(root).descendants(),
      links = tree(root).descendants().slice(1);

  d3.select("svg").transition()
      .duration(duration)
      .attr("height", height);

  d3.select(self.frameElement).transition()
      .duration(duration)
      .style("height", height + "px");

      var index = -1;

      root.eachBefore(function (n) {
        n.x = ++index * barHeight;
        n.y = n.depth * 40;
      });


  // Update the nodes…
  var node = svgGroup.selectAll("g.node")
      .data(nodes, function(d) {
         return d.id || (d.id = ++i);
       })

  if(enableDrag){
      nodeEnter = node.enter().append("g")
        .call(dragListener);
    }
    else{
      nodeEnter = node.enter().append("g");
    }

    nodeEnter.attr("class", "node")
      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
      .style("opacity", 1);

  //append circle
   nodeEnter.append("circle")
            .attr('class', 'nodeCircle')
            .attr("r", 0)
            .style("fill", function(d) {
                return d._children ? "lightsteelblue" : "#fff";
            });


  // Enter any new nodes at the parent's previous position.
 nodeEnter.append("rect")
      .attr("y", -barHeight / 2)
      .attr("x", -15)
      .attr("height", barHeight - 1)
      .attr("width", function(d){
        if(d.data.key)
          return (d.data.key.length + 12) * 6;
          else
            return 120;
      })
      .style("fill", function(d){

          if(enableDrag == false){
            if( refTreeNames.indexOf(d.data.key) > -1)
              return "rgb(216, 206, 196)";
            else
                return "rgb(204, 235, 197)";
              }

          if(baseTreeNames.length > 0){
              if( baseTreeNames.indexOf(d.data.key) > -1)
                return "rgb(216, 206, 196)";
              else{
                return "rgb(204, 235, 197)";
              }
          }else {
              return "rgb(216, 206, 196)";
          }
      })
      .on("click", click);

  nodeEnter.append("text")
      .attr("dy", 3.5)
      .attr("dx", -5)
      .style("font-size", "18px")
      .text(function (d) {
        if (d._children === null) {
          if (d.data.key)
            return ' - ' + d.data.key;
          else
            return ' - ' + 'unclassified';
        } else if (d.children === null) {
          if (d.data.key)
            return ' + ' + d.data.key;
          else
            return ' + ' + 'unclassified';
        } else {
          return d.data.key;
        }
      });

  if(enableDrag == false){
  // phantom node to give us mouseover in a radius around it
      nodeEnter.append("circle")
            .attr('class', 'ghostCircle')
            .attr("r", 25)
            .attr("opacity", 0.2) // change this to zero to hide the target area
        .style("fill", "red")

      nodeEnter.selectAll('rect')
      .attr('pointer-events', 'mouseover')
            .on("mouseover", function(node) {
                overCircle(node);
            })
            .on("mouseout", function(node) {
                outCircle(node);
            });
  }


  if(enableDrag == false){
   // Add a context menu
   node.on('contextmenu', d3.contextMenu(menu));
  }

  /* Change the circle fill depending on whether it has children and is collapsed
        node.select("circle.nodeCircle")
            .attr("r", 4.5)
            .style("fill", function(d) {
                return d._children ? "lightsteelblue" : "#fff";
            });
*/
  // Transition nodes to their new position.
  nodeEnter.transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
      .style("opacity", 1);

  node.transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
      .style("opacity", 1)
    .select("rect")
      .style("fill", "#d8cec4");

  // Transition exiting nodes to the parent's new position.
  node.exit().transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
      .style("opacity", 1e-6)
      .remove();


  // Update the links…
  // Update the links…
  var link = svgGroup.selectAll(".link")
    .data(root.links(), function (d) {
      return d.target.id;
    });

  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
    .attr("class", "link")
    .style("fill", "none")
    .style("stroke", "grey")
    .attr("d", function (d) {
      var o = {x: source.x0, y: source.y0, parent: {x: source.x0, y: source.y0}};
      return elbow({
        source: o,
        target: o
      });
    })
    .transition()
    .duration(duration)
    .attr("d", elbow);

  // Transition links to their new position.
  link.transition()
    .duration(duration)
    .attr("d", elbow)
    .style("fill", "none")
    .style("stroke", function(d) {
      if (d.target.class === "found") {
        return "#4cff00";
      }else {
        return "grey"
      }
    })
    .style("stroke-opacity", function(d) {
      if (d.target.class === "found") {
        return 0.7;
      }
    })
    .style("stroke-width", function(d) {
      if (d.target.class === "found") {
        return ".35rem";
      }
    })
    .style("fill-opacity", function(d) {
      if (d.target.class === "found") {
        return 0.70;
      }
    });
    //.style("stroke-width", "2px");

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
    .duration(duration)
    .attr("d", function (d) {
      var o = {
        x: source.x,
        y: source.y
      };
      return elbow({
        source: o,
        target: o
      });
    })
    .remove();

  // Stash the old positions for transition.
  nodes.forEach(function (d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });


}//end of update

  function openPaths(paths){
		for(var i =0;i<paths.length;i++){
			if(paths[i].id !== "1"){//i.e. not root
				paths[i].class = 'found';
				if(paths[i]._children){ //if children are hidden: open them, otherwise: don't do anything
					paths[i].children = paths[i]._children;
	    			paths[i]._children = null;
				}
				update(paths[i]);
			}
		}
	}

//basically a way to get the path to an object
	function searchTree(obj,search,path){
  //  console.log(obj);
		if(obj.data.key === search){ //if search is found return, add the object to the path and return it
			path.push(obj);
			return path;
		}
		else if(obj.children || obj._children){ //if children are collapsed d3 object will have them instantiated as _children
			var children = (obj.children) ? obj.children : obj._children;
			for(var i=0;i<children.length;i++){
				path.push(obj);// we assume this path is the right one
				var found = searchTree(children[i],search,path);
				if(found){// we were right, this should return the bubbled-up path from the first if statement
					return found;
				}
				else{//we were wrong, remove this parent from the path and continue iterating
					path.pop();
				}
			}
		}
		else{//not the right object, return false so it will continue to iterate in the loop
			return false;
		}
	}

  function reSortRoot(root,value_key) {
  		//console.log("Calling");
  		for (var key in root) {
  			if (key == "key") {
  				root.name = root.key;
  				delete root.key;
  			}
  			if (key == "values") {
  				root.children = [];
  				for (item in root.values) {
  					root.children.push(reSortRoot(root.values[item],value_key));
  				}
  				delete root.values;
  			}
  			if (key == value_key) {
  				root.value = parseFloat(root[value_key]);
  				delete root[value_key];
  			}
  		}
  		return root;
  	}

	function extract_select2_data(node,leaves,index){

	        if (node.values){
	            for(var i = 0;i<node.values.length;i++){
	                index = extract_select2_data(node.values[i],leaves,index)[0];
	            }
	        }
	        else {
              if(node.key != "")
	             leaves.push({id:++index,text:node.key});
	        }
	        return [index,leaves];
	}

  function openPaths(paths){
  		for(var i =0;i<paths.length;i++){
  			if(paths[i].id !== "1"){//i.e. not root
  				paths[i].class = 'found';
  				if(paths[i]._children){ //if children are hidden: open them, otherwise: don't do anything
  					paths[i].children = paths[i]._children;
  	    			paths[i]._children = null;
  				}
  				update(paths[i]);
  			}
  		}
  	}

  /*
    function zoom() {
		    svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
	   }

    function centerNode(source, divId) {
        scale = zoomListener.scale();
		x = -source.y0;
		y = -source.x0;
		x = x * scale + width / 8 ;
		y = y * scale + height / 2;


        d3.select("#"+divId).select('g').transition()
		  .duration(duration)
		  .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");

        zoomListener.scale(scale);
		zoomListener.translate([x, y]);
    }
*/
/* Toggle children on click.
function click(d) {

  chartObj.forEach(function(obj_data){

    var newPath = getNodePath(root, d.id);
    console.log(newPath)
    if (newPath.children) {

      newPath._children = newPath.children;
      newPath.children = null;
    } else {
      newPath.children = newPath._children;
      newPath._children = null;
    }

    update(obj.__data__);
    //update(root)
    //centerNode(newPath, obj.id);

  });

}//end of click
*/

function click(d) {
    console.log(d);
    if (d.children) {
	     d._children = d.children;
	     d.children = null;
    } else {
	     d.children = d._children;
	     d._children = null;
    }
    update(d);

}


  outer_update = update;

  // To get path of node of each chart
    // Receives two argument 1 : data obejct and
    // 2 : id to be searched i.e. clicked node
    function getNodePath(dataObj, searchId){
      console.log(dataObj);
        // Check for id of clicked node and current chart object data
        if(searchId === dataObj.id ){

            return dataObj;
        }
        else{
            if(dataObj.children){

                for( var i = 0; i < dataObj.children.length; i++){
                    var path = getNodePath(dataObj.children[i], searchId);

                    if(path){
                        return path;
                    }

                }
            }
        }
    }//end of getNodePath

    // Function to get same color for child based on color of parent.
    // Using d3.hsl as color palette
    function getColor(d) {

      if (d.depth == 0) {
        return "white";
      }
      var fadeColor = 1;

      while (d.depth > 2) {
        d = d.parent;
      }
      var c = d3.lab(color(d.data.key))
      //.brighter();
      return c;
    }

function elbow(d, i) {
    //divide  d.source.y and d.target.y by 2 to reduce link distance
    //also change the node transform to scale with link distance
    return "M" + (d.source.y) + "," + d.source.x
      + "V" + (d.target.x ) + "H" + d.target.y

}

function getBaseName(tree_root){
  refTreeNames.push(tree_root.key)
  if(tree_root.children)
    tree_root.children.forEach(getBaseName);

}

 // collapse everything
            function collapseAll(d) {
              //console.log(d);
                if (d.children && d.children.length === 0) {
                    d.children = null;
                }
                if (d.children) {
                    d._children = d.children;
                    d._children.forEach(collapseAll);
                    d.children = null;
                }
            } // end of collapse function

 // Expands a node for i levels
    function expand(d, i) {
        var local_i = i;
        if (typeof local_i === "undefined") {
            local_i = 1;
        }
        if (local_i > 0) {
            if (d._children) {
                d.children = d._children;
                d._children = null;
            }
            if (d.children) {
                d.children.forEach(function (c) {expand(c, local_i - 1); });
            }
        }
    }//end of expand function

  // Define the zoom function for the zoomable tree

   // Function to update the temporary connector indicating dragging affiliation
    var updateTempConnector = function() {
        var data = [];
        if (draggingNode !== null && selectedNode !== null) {
        //  console.log(selectedNode);
        //  console.log(draggingNode);
            // have to flip the source coordinates since we did this for the existing connectors on the original tree
            //console.log( selectedNode);
            data = [{
                source: {
                    x:   selectedNode.y0 ,
                    y:   selectedNode.x0
                },
                target: {
                    x:  selectedNode.y0 ,
                    y:  selectedNode.x0
                }
            }];
        }
        //console.log(data);
        var link = d3.select("g#reftreeSvgGroup").selectAll(".templink").data(data);

        link.enter().append("path")
            .attr("class", "templink")
            .attr("d", elbow)
            .attr('pointer-events', 'none');

        link.attr("d", elbow);

        link.exit().remove();
    };


    function zoom() {
        svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }

    function diffColor(){

        baseTreeNodes = d3.select("basetreeSvgGroup").selectAll("node");
        console.log(baseTreeNodes);
    }

    // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
    //var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

    function initiateDrag(d, domNode) {
        draggingNode = d;
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle show');
        d3.select(domNode).attr('class', 'node activeDrag');

        svgGroup.selectAll("g.node").sort(function(a, b) { // select the parent and sort the path's
            if (a.id != draggingNode.id) return 1; // a is not the hovered element, send "a" to the back
            else return -1; // a is the hovered element, bring "a" to the front
        });
        // if nodes has children, remove the links and nodes
        if (dragNodedata.length > 1) {

            // remove link paths
            //links = tree(nodes);  //d3.v3
            //links = nodes.links();    //d3.v4
            let links = tree(draggingNode).links(); //d3.v4

            let nodePaths = svgGroup.selectAll("path.link")
                .data(links, function(d) {
                    return d.target.id;
                }).remove();

            // remove child nodes
            nodesExit = svgGroup.selectAll("g.node")
                .data(dragNodedata, function(d) {
                    return d.id;
                }).filter(function(d, i) {
                    if (d.id == draggingNode.id) {
                        return false;
                    }
                    return true;
                }).remove();
        }

        // remove parent link
        //  parentLink = tree.links(tree.nodes(draggingNode.parent)); //d3.v3
        parentLink = tree(draggingNode.parent).links(); //d3.v4

        svgGroup.selectAll('path.link').filter(function(d, i) {
          //console.log(draggingNode);
            if (d.target.id == draggingNode.id) {
                return true;
            }
            return false;
        }).remove();

        dragStarted = null;
    }
    // Define the drag listeners for drag/drop behaviour of nodes.
    dragListener = d3.drag()
        .on("start", function(d) {
            if (d == root) {
                return;
            }

            dragStarted = true;
            //nodes = tree.nodes(d);  //d3v3 version
            dragNodedata = tree(d).descendants(); //d3v4 version
            d3.event.sourceEvent.stopPropagation();
            // it's important that we suppress the mouseover event on the node being dragged. Otherwise it will absorb the mouseover event and the underlying node will not detect it d3.select(this).attr('pointer-events', 'none');
        })
        .on("drag", function(d) {
            if (d == root) {
                return;
            }

            if (dragStarted) {
              d3.select("#basetree").style("overflow","visible")
                domNode = this;
                initiateDrag(d, domNode);
            }

            // get coords of mouseEvent relative to svg container to allow for panning
            relCoords = d3.mouse($('svg').get(0));
            if (relCoords[0] < panBoundary) {
                panTimer = true;
                pan(this, 'left');
            } else if (relCoords[0] > ($('svg').width() - panBoundary)) {

                panTimer = true;
               // pan(this, 'right');
            } else if (relCoords[1] < panBoundary) {
                panTimer = true;
               // pan(this, 'up');
            } else if (relCoords[1] > ($('svg').height() - panBoundary)) {
                panTimer = true;
                //pan(this, 'down');
            } else {
                try {
                    clearTimeout(panTimer);
                } catch (e) {

                }
            }

            d.x0 += d3.event.dy;
            d.y0 += d3.event.dx;
            //console.log(d.x0 +' '+ d.y0);
            var node = d3.select(this);
            node.attr("transform", "translate(" + d.y0 + "," + d.x0 + ")");
            updateTempConnector();
        }).on("end", function(d) {
            if (d == root) {
                return;
            }

           d3.select("#basetree").style("overflow","auto")

            domNode = this;

            if (selectedNode && draggingNode && selectedNode != draggingNode) {
                  // now remove the element from the parent, and insert it into the new elements children
                  var index = draggingNode.parent.children.indexOf(draggingNode);
                  if (index > -1) {
                      draggingNode.parent.children.splice(index, 1);
                      if (draggingNode.parent.children.length == 0) {
                          draggingNode.parent.children = null;
                      }
                  }
                  if (selectedNode.children !== null && (typeof selectedNode.children !== 'undefined' || typeof selectedNode._children !== 'undefined')) {
                      if (typeof selectedNode.children !== 'undefined') {
                          selectedNode.children.push(draggingNode);
                        draggingNode.parent = self.selectedNode;
                      } else {
                          selectedNode._children.push(draggingNode);
                      }
                  } else {
                      selectedNode.children = [];
                      selectedNode.children.push(self.draggingNode);
                      self.draggingNode.parent = self.selectedNode;
                  }

                // Make sure that the node being added to is expanded so user can see added node is correctly moved
                //expand(selectedNode);

                endDrag();
            } else {
                endDrag();
            }
        });

    function endDrag() {
        selectedNode = null;
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle');
        d3.select(domNode).attr('class', 'node');
        // now restore the mouseover event or we won't be able to drag a 2nd time
      d3.select(domNode).select('.ghostCircle').attr('pointer-events', '');
      // d3.select(domNode).selectAll('circle').attr('pointer-events', '');
        updateTempConnector();
        if (draggingNode !== null) {
            update(draggingNode);
           // centerNode(draggingNode);
            draggingNode = null;
        }
    }

    // Function to center node when clicked/dropped so node doesn't get lost when collapsing/moving with large amount of children.

    function centerNode(source) {
        scale = zoomListener.scale();
        x = -source.y0;
        y = -source.x0;
        x = x * scale + viewerWidth / 2;
        y = y * scale + viewerHeight / 2;
        d3.select('g').transition()
            .duration(duration)
            .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
        zoomListener.scale(scale);
        zoomListener.translate([x, y]);
    }

    function setSourceFields(child, parent) {
        if (parent) {
            for (var i = 0; i < sourceFields.length; i++) {
                var sourceField = sourceFields[i];
                if (child[sourceField] != undefined) {
                    child["source_" + sourceField] = child[sourceField];
                }
                parent["source_" + sourceField] = (child["source_" + sourceField]) ? child["source_" + sourceField] : child[sourceField];
            }
        }

    }

    function getNodeNames(node, leaves, index){

      if (node.values){
          for(var i = 0;i<node.values.length;i++){
             leaves.push({id:++index,text:node.key});
              index = getNodeNames(node.values[i],leaves,index)[0];
          }
      }
      else {
          if(node.key != "")
           leaves.push({id:++index,text:node.key});
      }
      return [index,leaves];

    }

    function sumNodes(nodes) {
        for (var y = 0; y < nodes.length; y++) {
            var node = nodes[y];
            if (node.children) {
                sumNodes(node.children);
                for (var z = 0; z < node.children.length; z++) {
                    var child = node.children[z];
                    for (var i = 0; i < sumFields.length; i++) {
                        if (isNaN(node["sum_" + sumFields[i]])) node["sum_" + sumFields[i]] = 0;
                        node["sum_" + sumFields[i]] += Number(child["sum_" + sumFields[i]]);
                        if ((node.parent)) {
                            levelCeil[node.depth-1]["sum_" + sumFields[i]] = Math.max(levelCeil[node.depth-1]["sum_" + sumFields[i]], Number(node["sum_" + sumFields[i]]));
                            setSourceFields(node, node.parent);
                        }

                    }
                }
            }
            else {
                for (var i = 0; i < sumFields.length; i++) {
                    node["sum_" + sumFields[i]] = Number(node[sumFields[i]]);
                    if (isNaN(node["sum_" + sumFields[i]])) {
                        node["sum_" + sumFields[i]] = 0;
                    }
                }
            }
            setSourceFields(node, node.parent);
        }

    }

    // A recursive helper function for performing some setup by walking through all nodes

    function visit(parent, visitFn, childrenFn) {
        if (!parent) return;

        visitFn(parent);

        var children = childrenFn(parent);
        if (children) {
            var count = children.length;
            for (var i = 0; i < count; i++) {
                visit(children[i], visitFn, childrenFn);
            }
        }
    }

    /* Call visit function to establish maxLabelLength
    visit(root, function(d) {
        totalNodes++;
        maxLabelLength = Math.max(d.name.length, maxLabelLength);

    }, function(d) {
        return d.children && d.children.length > 0 ? d.children : null;
    });
*/
    function delete_node(node) {
            visit(root, function(d) {
                   if (d.children) {
                           for (var child of d.children) {
                                   if (child == node) {
                                     console.log(node);
                                           d.children = _.without(d.children, child);
                                           d.children = null;
                                           update(root);
                                           break;
                                   }
                           }
                   }
            },
            function(d) {
               return d.children && d.children.length > 0 ? d.children : null;
           });
        }

    //color Function
    /*
      create array of name from the ref Tree
      iterate the base tree and check for the names in the refTreeNames
      if a name in the base tree doesn't matches the call colorDiifNodes()
      for that node upto parent
    */

   //export function to modules
    exports.width = function(_){
        if(!arguments.length) return width;
        width = _;
        return exports;
    }

    exports.height = function(_){
        if(!arguments.length) return height;
        height = _;
        return exports;
    }

    exports.enableDrag = function(_){
		if(!arguments.length) return enableDrag;
		enableDrag = _;
		return exports;
	}

    //d3.rebind(exports, dispatch, "on");
    return exports;
}//end of module
