# taxonEditor

This web application enables a user to visualy compare and edit working taxonomy tree with a given reference taxonomy  

## Features
* Drag And Drop
* Tool-tip With Edit Methods
* Synchronous/independent scroll
* Node Coloring

## Project Status
The project is still in active development.


## Demo
[Prototype Demo](http://tomarashish.gitlab.io/taxonomyCompare/)

### Screenshot of taxonomy editor
![Current Version With DWCA file format](data/drag_n_drop2.gif)

### Presentation
[Medium Blog](https://medium.com/@mithyatva/gbif-ebbe-nielsen-challenge-2018-taxonomy-tree-editor-baf3ca538e49)

### Completed
- [x] Interactive front-end for displaying reference and working taxonomy trees
- [x] Drag and drop of a node from reference taxonomy to working taxonomy
- [x] Search child node by using auto-complete feature
- [x] Synchronous and independent scroll option for simultaneous visualization of taxonomy tree
- [x] Right click over a node in working taxonomy displays a tooltip with editing features
- [x] Node color Option: Differential coloring of tree nodes to compare presence and absence of nodes in reference and working taxonomy.

### TODO
- [ ] Working taxonomy node editing feature which provides one click method for editing node (create, rename and delete).  
- [ ] Highlight sub tree by right click on node of working taxonomy.
- [ ] Load, download and load example buttons to enable user to add and download taxonomy tree.


## Credits
A heartfelt thanks to [Fredrik Ronquist](https://ronquistlab.github.io/people.html) for his valuable guidance and support

## Funding
This project has received funding from the European Union’s Horizon 2020 research and innovation programme under the Marie Sklodowska-Curie grant agreement No 642241.
